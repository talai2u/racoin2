<?php 

class Controller{

	private $app;
	private $req;
	private $resourceUri;
	private $param;

	public function __construct($param=null){
		$this->app = \Slim\Slim::getInstance();
		$this->req = $this->app->request;
		$this->resourceUri = $this->req->getResourceUri();
		$this->param = $param;
	}

	public function showHomePage(){

		$cat = Category::getAll();

		$dep = City::getAll();

		$v = new ViewHome($cat,$dep);	
		echo $v->display();
	}

	public function showSearchPage(){

		$cat = Category::getAll();
		$dep = City::getAll();
		$annonce= Annonce::getAll();
		$v = new ViewSearch($cat,$dep,$annonce);	
		echo $v->display();

	}
	public function annonceByCategory($id){
		$c = Category::find($id) ;
		
		$annonces = $c->annonces->toArray() ;

		$v = new ViewListByCat($annonces);
		echo $v->display();
		
	}

	public function annonceList(){

		$annonces = Annonce::getAll();
		$images = Array();
		foreach($annonces as $annonce){
			$images[] =Image::where('id_annonce' , '=' , $annonce['id'])->first();
			$city[]=City::where('id' , '=' , $annonce['id_city'])->first();
		    }
		$v = new ViewList($annonces,$images,$city);
		echo $v->display();
	}

	public function addAnnoncePage(){
		$cat = Category::getAll();
		$dep = City::getAll();
		$v = new ViewAdd($cat,$dep);	
		echo $v->display();
	}

	public function addPreview(){
		$a = new Annonce();
		$a->title=$this->req->post('titre');
		$a->description=$this->req->post('desc');
		$a->price=$this->req->post('price');
		$a->date=date("Y-m-d");
		$a->tel=$this->req->post('tel');
		$a->mail=$this->req->post('mail');
		$a->mot_passe=$this->req->post('mot_passe');
		$a->id_user=$_SESSION['id'];
		$c = Category::find($this->req->post('category'));
		$a->cat=$c->name;
		$a->id_cat=$this->req->post('category');
		$v = City::find($this->req->post('city'));
		$a->city=$v->name;
		$a->id_city=$this->req->post('city');
		$v = new ViewPreview($a);
		echo $v->display();
		$_SESSION['annonce']=$a;

	}

	public function addAnnonce(){
		$a = new Annonce();
		$a->date=date('d/m/Y');
		$a->title = $this->req->post('titre');
		$a->description=$this->req->post('desc');
		$a->price=$this->req->post('price');
		$a->id_category=$this->req->post('category');
		$a->id_city=$this->req->post('city');
		dd($_POST['firstname']);
		if(isset($_SESSION['login'])){		
	       $u=User::findByUserName($_SESSION['login']);
           $a->id_user = $u->id;
           $a->id_category = $u->category;
		}else{			
			$newUser = new User();
			$newUser->firstname = $this->req->post('firstname');
			$newUser->lastname = $this->req->post('lastname');
			$newUser->email = $this->req->post('email');
			$newUser->phone = $this->req->post('phone');
			$newUser->save();
			$a->id_user = $newUser->id;
			$salt = crypt(time());
		    $hash = crypt($this->req->post('mot_passe'), $salt);
		    $a->mot_passe=$hash;
		    $a->salt = $salt;
		}
		$a->save();
		if(isset($_FILES['file_array'])){
			$name_array = $_FILES['file_array']['name']; 
			$tmp_name_array = $_FILES['file_array']['tmp_name'];
			$type_array = $_FILES['file_array']['type']; 
			$size_array = $_FILES['file_array']['size'];
			$error_array = $_FILES['file_array']['error'];
			if ($a->save()) {
				for($i = 0; $i < count($tmp_name_array); $i++){
					$src="app/views/img/annonces/".$name_array[$i];
					if(move_uploaded_file($tmp_name_array[$i], $src )){
							$img = new Image();
							$img->src=$src;
							$img->id_annonce=$a->id;
							$img->save();
						
					}else { 
						echo "move_uploaded_file function failed for ".$name_array[$i]."<br>"; 
					}
				}
				
				$this->showHomePage();
			}
		}


	}


	public function updateAnnoncePage($id){
        $annonce = Annonce::Find($id)->toArray();
        $cat = Category::getAll();
		$dep = City::getAll();
		$v = new ViewUpdate($cat,$dep,$annonce);	
		echo $v->display();
	}
	public function updateAnnonce($id){
		$a = Annonce::Find($id);
		$a->title=$this->req->post('titre');
		$a->description=$this->req->post('desc');
		$a->price=$this->req->post('price');
		$a->id_category=$this->req->post('category');
		$a->id_city=$this->req->post('city');
		$a->save();
		$this->app->redirect("/racoin2/annonce/$id");

	}

	public function myAdsList($username){

	    $u=User::findByUserName($username);
		$annonces = $u->annonces->toArray();
		if($annonces){
		foreach($annonces as $annonce){
			$images[] =Image::where('id_annonce' , '=' , $annonce['id'])->first();
		    $city[]=City::where('id' , '=' , $annonce['id_city'])->first();
		 }
		$v = new ViewMyAds($annonces,$images,$city);
		echo $v->display();
		}
		else{
			$msg="inconnu";
			$v = new ViewMyAds($msg);
		echo $v->display();
		}
		
		
	}


	public function annonceDetail($id){
		$param = array();
		$param['annonce'] = Annonce::find($id);
		$img =Image::where('id_annonce' , '=' , $id)->get();
		$param['category'] = $param['annonce']->category->toArray() ;
		$param['user'] =$param['annonce']->user->toArray();
		$param['city'] =$param['annonce']->city->toArray();
		$param['image']=$img->toArray();
		$v = new ViewDetail($param);
		echo $v->display();
		
	}
	public function annonceSearch(){
		$annonces = Annonce::search($this->req->post('mots'));
		$cat = Category::getAll();
		$dep = City::getAll();

		$v = new ViewSearch($cat,$dep,$annonces);	
		echo $v->display();
	}

	public function showLogin(){
		$v = new ViewLogin();
		echo $v->display();
	}


	public function showNewAccount(){
		$cat = Category::getAll();
		$v = new ViewNewAccount($cat);
		echo $v->display();
	}
	public function addUser(){
		$u = new User();
		// hashage
		$salt = crypt(time());
		$hash = crypt($this->req->post('password'), $salt);
		$u->password=$hash;
		$u->salt = $salt;
		$u->firstname=$this->req->post('firstname');
		$u->lastname=$this->req->post('lastname');
		$u->email=$this->req->post('email');
		$u->phone=$this->req->post('phone');
		$u->category=$this->req->post('category');
		$u1=User::findByUserName($u->email);
		if($u1!=null){
			$cat = Category::getAll();
			$msg='Identifiant existant';
			$v = new ViewNewAccount($cat,$msg);
		    echo $v->display();
		}
		else{
		$u->save();
		$c = Category::Find($u->category);
		$v = new ViewUserProfil($u,$c->name);
		echo $v->display();
		}
	}

	public function verifyMotpasse($id){

        $annonce = Annonce::Find($id);
        if(!empty($this->req->post('Updatepassword'))){
        	
          $hash = crypt($this->req->post('Updatepassword'), $annonce->salt);
          if($hash==$annonce->mot_passe){
	        $annonce = $annonce->toArray();
            $cat = Category::getAll();
		    $dep = City::getAll();
		    $v = new ViewUpdate($cat,$dep,$annonce);
		    echo $v->display();	
		   }else{
				$e = ' Mot de passe erroné ';
				$this->showError($e);
		   }
	    }
		else if(!empty($this->req->post('deletepassword'))){
			 $hash = crypt($this->req->post('deletepassword'), $annonce->salt);
			if($annonce->mot_passe==$hash){
	            $annonce->delete();
	            $this->annonceList();
		   }else{
				$e = ' Mot de passe erroné ';
				$this->showError($e);
			}
		}
		else{
			if ((isset($_SESSION['admin'])) && ($_SESSION['admin']==1)){
				$annonce->delete();
				$e = "L'annonce a été supprimée.";
				$this->showError($e);
			}
		}
	}



	public function login(){
		
			$username=$this->req->post('login');
			$pwd=$this->req->post('password');
			$u=User::findByUserName($username);

			$hash = crypt($pwd, $u->salt);

		  	if($hash == $u->password){
				//session_start();
				$_SESSION['login'] = $u->email;
				$_SESSION['id']=$u->id;
				$_SESSION['admin'] = $u->admin;
				$_SESSION['nom'] = $u->firstname." ".$u->lastname;
				$this->username = $_SESSION['login'] ;
				$this->logged_in = true;
				$this->app->redirect("/racoin2");

			}
			
		

	}	

	public function logout(){
		$this->logged_in = false;
		session_destroy(); 
		$this->app->redirect("/racoin2");
	}

	public function admin(){
		if ((isset($_SESSION['admin'])) && ($_SESSION['admin']==1)){
			$v = new ViewNewAccount();
			echo $v->display();
		}else{
			$e = "Veuillez vous connecter en tant qu'administrateur.";
			$this->showError($e);
		}
		
	}

	public function newAdmin(){
		if ((isset($_SESSION['admin'])) && ($_SESSION['admin']==1)){	
			$temp= User::findByUserName($this->req->post('login'));
			// si pas d'utilisateur existant avec ce login
			if(is_null($temp)){
				$u = new User();
				$u->login=$this->req->post('login');
				// hashage
				$salt = crypt(time());
				$hash = crypt($this->req->post('password'), $salt);
				$u->password=$hash;
				$u->salt = $salt;
				$u->firstName=$this->req->post('firstname');
				$u->lastName=$this->req->post('lastname');
				$u->email=$this->req->post('email');
				$u->phone=$this->req->post('phone');
				$u->admin=1;
				$u->save();
				
				$this->newAdminRecap($u);
			}else{
				// affichage d'une erreur qui un compte existe déjà
				$e = "Un utilisateur existe déjà pour le login fournit.";
				$v = new ViewNewAccount($e);
				echo $v->display();
			}
		}else{
			$e = "Veuillez vous connecter en tant qu'administrateur.";
			$this->showError($e);
		}
	}

	public function newAdminRecap($u=null){
		if ((isset($_SESSION['admin'])) && ($_SESSION['admin']==1)){
			if(isset($u)){
				$v = new ViewAdminRecap($u);
				echo $v->display();
			}else{
				$e = "Une erreur s'est produite.";
				$this->showError($e);
			}
		}else{
			$e = "Veuillez vous connecter en tant qu'administrateur.";
			$this->showError($e);
		}
	}

	public function showError($erreur){
		$v = new ViewError($erreur);
		echo $v->display();
	}

	public function newCat(){
		if ((isset($_SESSION['admin'])) && ($_SESSION['admin']==1)){
			$v = new ViewNewCat();
			echo $v->display();
		}else{
			$e = "Veuillez vous connecter en tant qu'administrateur.";
			$this->showError($e);
		}		
	}

	public function addCat(){
		$c = new Category();
		$c->name = $this->req->post('label');
		$c->save();
		$msg = "La catégorie ".$c->name." a été créée.";
		$this->showError($msg);
	}

	public function userProfil(){
		$u= User::findByUserName($_SESSION['login']);
		$c = Category::Find($u->category);
		$v= new ViewUserProfil($u,$c->name);
		$v->display();
	}
}

	