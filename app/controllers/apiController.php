<?php 

class apiController{

	private $app;
	private $req;
	private $resourceUri;
	private $param;

	public function __construct($param=null){
		$this->app = \Slim\Slim::getInstance();
		$this->req = $this->app->request;
		$this->resourceUri = $this->req->getResourceUri();
		$this->param = $param;
	}

	public function authorization($route){
		$key = $route->getParams();
		$apikey = Api::verifyKey($key['key']);
		if (!$apikey) {
			
			$this->app->response()->header('Content-Type', 'application/json');
			$this->app->halt(401,json_encode(array("status" => 401,
													"message" => "bad authentification Racoin api"
												), JSON_PRETTY_PRINT));
			

		}
	}

	public function apiAnnonceList(){
		
		$annonces = Annonce::apiGetAll();
		$this->app->response()->header('Content-Type', 'application/json');
		
		$response = array();
		$annoncesTab = array();
		
		foreach ($annonces as $key => $a) {
			
			$tab = array("annonce" => array(
					"id" => $a["id"],
					"title" => $a["title"]
			),
			"links" => array(
					array(
					"rel" => "self",
					"href" => $this->req->getUrl().$this->req->getRootUri()."/api/annonce/".$a["id"]
					))
					);


			array_push($response, $tab);
		}

		
		
		echo json_encode(array("annonces" => $response), JSON_PRETTY_PRINT |  JSON_UNESCAPED_SLASHES);
		
	}

	public function apiAnnonce($id){

		
		$ad = Annonce::getById($id);
		$annonce = Annonce::apiGetById($id);
		
		$category = $ad->apiCategory();
		
		$this->app->response()->header('Content-Type', 'application/json');
		
		$response = array();
		$annoncesTab = array();
		
			
			$tab = array("annonce" => array(
					"id" => $annonce["id"],
					"title" => $annonce["title"],
					"date" => $annonce["date"],
					"description" => $annonce["description"],
					"price" => $annonce["price"],
					"category" => array(
									"id" => $category["id"],
									"name" => $category["name"],
									"href" => $this->req->getUrl().$this->req->getRootUri()."/api/category/".$category["id"]
								),
					"id_city" => $annonce["id_city"],
					"id_user" => $annonce["id_user"]
				));


			array_push($response, $tab);
		
		echo json_encode($tab, JSON_PRETTY_PRINT |  JSON_UNESCAPED_SLASHES);
		
	}

	public function apiCategoryList(){
		
		$cats = Category::apiGetAll();
		$this->app->response()->header('Content-Type', 'application/json');
		
		$response = array();
		$annoncesTab = array();
		
		foreach ($cats as $key => $a) {
			
			$tab = array("category" => array(
					"id" => $a["id"],
					"name" => $a["name"]
			),
			"links" => array(
					array(
					"rel" => "self",
					"href" => $this->req->getUrl().$this->req->getRootUri()."/api/category/".$a["id"]
					))
					);


			array_push($response, $tab);
		}

		
		
		echo json_encode(array("categories" => $response), JSON_PRETTY_PRINT |  JSON_UNESCAPED_SLASHES);
		
	}

	public function apiAdsByCat($id){
		$cat = Category::getById($id);
		$annonces = $cat->apiAnnonces();
		

		$this->app->response()->header('Content-Type', 'application/json');
		
		$response = array();
		$annoncesTab = array();
		
		foreach ($annonces as $key => $a) {
			
			$tab = array("annonce" => array(
					"id" => $a["id"],
					"name" => $a["title"],
					"href" => $this->req->getUrl().$this->req->getRootUri()."/api/annonce/".$a["id"]
			));


			array_push($response, $tab);
		}

		
		
		echo json_encode(array("Annonces" => $response), JSON_PRETTY_PRINT |  JSON_UNESCAPED_SLASHES);
		
	}


}

