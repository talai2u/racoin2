<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
class Annonce extends Eloquent{
	protected $table = 'annonce';
	protected $primaryKey = 'id';
	public $timestamps=false;


	public static function getAll()
	{
		return Annonce::all()->toArray();
	}

	public function category() {
		return $this->belongsTo( 'Category', 'id_category' );
	}

	public function user() {
        return $this->belongsTo( 'User', 'id_user' );
    }

    public function city() {
        return $this->belongsTo( 'City', 'id_city' );
    }

    public function image() {
        return $this->hasMany( 'image', 'id_annonce' );
    }

    public static function Search($mot) {
        
 	$query = Annonce::where('title', 'like', '%'.($mot).'%');
	
	// $query->addWhere('address', 'like', '%nancy%') ;
	
	$annonces = $query->get();

		if($annonces)
        	return $annonces;
    	else
    		return false;
    }
    public static function getById($id){
		return Annonce::find($id);
	}
    public static function apiGetAll()
	{
		return Annonce::all()->toArray();
	}
	public static function apiGetById($id)
	{
		return Annonce::find($id)->toArray();
	}

	public function apiCategory() {
		return $this->belongsTo( 'Category', 'id_category' )->first()->toArray();
	}


}