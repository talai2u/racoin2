<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
class Api extends Eloquent{
	protected $table = 'api';
	protected $primaryKey = 'key';
	public $timestamps=false;


	public static function verifyKey($key){

		return Api::find($key);
	}

}