<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
class Category extends Eloquent{
	protected $table = 'category';
	protected $primaryKey = 'id';
	public $timestamps=false;

	public static function getAll(){
		return Category::all()->toArray();
	}
	public function annonces() {
		return $this->hasMany( 'Annonce', 'id_category' ) ;
	}
	public static function getById($id){
		return Category::find($id);
	}
	public function apiAnnonces() {
		return $this->hasMany( 'Annonce', 'id_category' )->get()->toArray();
	}

	public static function apiGetAll(){
		return Category::all()->toArray();
	}

}