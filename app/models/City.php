<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
class City extends Eloquent{
	protected $table = 'city';
	protected $primaryKey = 'id';
	public $timestamps=false;


	public static function getAll(){
		return City::orderBy('name')->get()->toArray();
	}

}