<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
class Image extends Eloquent{
	protected $table = 'image';
	protected $primaryKey = 'id';
	public $timestamps=false;
	 public function annonce() {
        return $this->belongsTo('annonce', 'id_annonce' );
    }
	

}