<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
class User extends Eloquent{
	protected $table = 'user';
	protected $primaryKey = 'id';
	public $timestamps=false;

	public static function findByUserName($email){

		$user = User::where('email', $email)->first();

		return $user;
	}

	public function annonces() {
		return $this->hasMany( 'Annonce', 'id_user' ) ;
	}

}