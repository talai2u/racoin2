<?php	
Abstract class ViewAbstract{
	
	protected $layout =null;
	protected $obj;
	protected $arrayVar;
	protected $app;
	
	
	public function __construct($o=null) { 

		$this->obj = $o;


	}
	
	public function addVar($var, $val) { 
	
		$this->arrayVar[$var]=$val; 
	}
	
	public function render() {
	
	
	$loader = new Twig_Loader_Filesystem('app/views');
	
	$twig = new Twig_Environment( $loader );
	
    $twig->addGlobal("session", $_SESSION);
	
	$tmpl = $twig->loadTemplate($this->layout);
	
	return $tmpl->render($this->arrayVar);
	}

	public function display() { 
	
		echo $this->render();

	}	
}