<?php 
class ViewAdd extends ViewHome{

	public function __construct($cat=null,$dep=null,$d=null){
		parent::__construct($app=null,$d=null);
		
		$this->layout = 'addAnnonce.twig';

		$this->arrayVar['title'] = 'Déposer une annonce';

		$categories = $cat;
		$departements = $dep;

		$this->arrayVar['cats']=$categories;
		$this->arrayVar['departements']=$departements;

		$this->arrayVar['js_links'] = array(
			array(
			'title' => 'jquery', 
			'src' => 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js' 
			)
			,array(
			'title' => 'costumJs', 
			'src' => 'http://localhost/racoin2/app/plugins/costum.js' 
			)
			);
	

	}
	
}