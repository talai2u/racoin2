<?php 
use Illuminate\Database\Capsule\Manager as DB;
class ViewDetail extends ViewHome{

	public function __construct($param=array()){
		parent::__construct($app=null,$d=null);
		if ((isset($_SESSION['admin'])) && ($_SESSION['admin']==1)){
			$this->layout = 'annonceDetailAdmin.twig';
		}else{
			$this->layout = 'annonceDetailModif.twig';
		}
		
		$this->layout = 'annonceDetail.twig';

		$this->arrayVar['title'] = 'Details d annonce';
		$this->arrayVar['js_links'] = array(
			array(
			'title' => 'jquery',
			'src' =>'https://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js'
			),
			array(
			'title' => 'lightBox', 
			'src' => 'http://localhost/racoin2/app/plugins/pack.js' 
			)
			,array(
			'title' => 'lightBox-mousewheel', 
			'src' => 'http://localhost/racoin2/app/plugins/mousewheel.pack.js' 
			)
			,array(
			'title' => 'lightBox-easing', 
			'src' => 'http://localhost/racoin2/app/plugins/easing.pack.js' 
			)
			,array(
			'title' => 'costumJs', 
			'src' => 'http://localhost/racoin2/app/plugins/costum.js' 
			)
			,
			array(
			'title' => 'imagelightjs',
			'src' =>'http://localhost/racoin2/app/plugins/pirobox_extended_feb_2011.js'
			)
			);

		$this->arrayVar['css_links'] = array(
			array(
			'title' => 'css', 
			'href' => 'http://localhost/racoin2/app/plugins/fancybox.css' 
			)
			,array(
			  'title' => 'lightimage',
			  'href' => 'http://localhost/racoin2/app/views/stylesheets/style1.css'
			)


			);
		
		
	    $this->arrayVar['annonce'] = $param['annonce'];
		$this->arrayVar['user'] = $param['user'];
		$this->arrayVar['category'] = $param['category'];
		$this->arrayVar['city'] = $param['city'];
		$this->arrayVar['image'] = $param['image'];

			
	}	

	
	
}
