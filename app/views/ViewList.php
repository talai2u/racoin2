<?php 
use Illuminate\Database\Capsule\Manager as DB;
class ViewList extends ViewMain{

	public function __construct($annonces,$images,$city){
		parent::__construct($app=null,$d=null);
		$this->layout = 'annonceList.twig';
		$this->arrayVar['annonces'] = $annonces;
		$this->arrayVar['images'] = $images;
		$this->arrayVar['city']=$city;
		
	}
	
}
