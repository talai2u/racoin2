<?php 
class ViewMain extends ViewAbstract{
	
	

	public function __construct(){
		parent::__construct($app=null,$d=null);
		
		// var_dump($this->app->urlFor('search'));
		
		$this->layout = 'main.twig';
		
		// $this->arrayVar['d'] = $d ;
		$this->app= \Slim\Slim::getInstance();

		$this->arrayVar['nav_link'] = array(
			array('text' => 'Accueil', 'href' => $this->app->urlFor('root'),'class' => 'menu_link'),
			array('text' => 'Voir annonces', 'href' => $this->app->urlFor('list'), 'class' => 'menu_link'),
			array('text' => 'Déposer une Annonce', 'href' => $this->app->urlFor('add'), 'class' => 'menu_link'),
			array('text' => 'Rechercher', 'href' => $this->app->urlFor('search'),'class' => 'menu_link')
			);

		// si log
		if(isset($_SESSION['admin'])){
			if($_SESSION['admin']==1){
				$this->arrayVar['nav_link'][] = array('text' => 'Ajouter Administrateur', 'href' => $this->app->urlFor('admin'),'class' => 'menu_link');
				$this->arrayVar['nav_link'][] = array('text' => 'Catégories', 'href' => $this->app->urlFor('newCat'),'class' => 'menu_link');
			}
			$this->arrayVar['nav_link'][] = array('text' => 'Logout', 'href' => $this->app->urlFor('logout'),'class' => 'menu_link');
			$this->arrayVar['nav_link'][] = array('text' =>"Vous etes ".$_SESSION['nom']."",'href' => $this->app->urlFor('profil'),'class' => 'menu_link');
			
		}else{ 
			$this->arrayVar['nav_link'][] = array('text' => 'Login', 'href' => $this->app->urlFor('login'),'class' => 'menu_link');
		}



		$this->arrayVar['css_link'] = array(
		array('href' => '/racoin2/app/views/stylesheets/grille.css'),
		array('href' => '//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css'),
		array('href' => '/racoin2/app/views/stylesheets/style.css')
		);

		

	}

}
