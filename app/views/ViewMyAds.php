<?php 
use Illuminate\Database\Capsule\Manager as DB;
class ViewMyAds extends ViewMain{

	public function __construct($annonces=null,$images=null,$city=null,$msg=null){
		parent::__construct($app=null,$d=null);
		$this->layout = 'myAds.html';
		$this->arrayVar['title'] = 'Mes annonces';
		$this->arrayVar['annonces'] = $annonces;
		$this->arrayVar['msg']=$msg;
		$this->arrayVar['city']=$city;

		$this->arrayVar['images']=$images;
		
	}
	
}
