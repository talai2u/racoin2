<?php 
class ViewSearch extends ViewHome{

	public function __construct($cat=null,$dep=null,$annonce=null){
		parent::__construct($app=null,$d=null);
		
		$this->layout = 'search.twig';
		$this->arrayVar['title'] = 'Rechercher des annonces';

		$categories = $cat;
		$departements = $dep;
		
		if($this->app->request->isGet())
		$annonces=$annonce;

		if($this->app->request->isPost())
		$annonces=$annonce;


		$this->arrayVar['cats']=$categories;
		$this->arrayVar['departements']=$departements;
		$this->arrayVar['annonces']=$annonces;
	
	}
	
}