<?php 
class ViewUpdate extends ViewHome{

	public function __construct($cat=null,$dep=null,$annonce=null){
		parent::__construct($app=null,$d=null);
		
		$this->layout = 'updateAnnonce.html';

		$this->arrayVar['title'] = 'Modifier une annonce';

		$categories = $cat;
		$departements = $dep;
		$annonc = $annonce;
		$this->arrayVar['cats']=$categories;
		$this->arrayVar['departements']=$departements;
		$this->arrayVar['annonce']=$annonc;
	
	}
	
}