<?php
require 'vendor/autoload.php';

//---------Slim-------------
global $app;
$app = new \Slim\Slim([
	'templates.path' =>'app/views/'
	]);



//--------Eloquent--------
require 'app/config/database.php';

session_start();




$app->get('/',function() use($app){

	
	$c = new Controller();
	$c->showHomePage();

})->name('root');



$app->get('/search',function() {
	
	
	$c = new Controller();
	$c->showSearchPage();
	
})->name('search');

$app->post('/search',function() {
	$c = new Controller();
	$c->annonceSearch();
	
})->name('searchPost');

$app->get('/profil', function(){
	$c = new Controller();
	$c->userProfil();
})->name('profil');
// --------------------
$app->get('/category/:id',function($id) use($app){
	
	$c = new Controller();
	$c->annonceByCategory($id);

})->name('cat');


//Affichage liste des annonces
$app->get('/list',function() {
	
	$c = new Controller();
	$c->annonceList();

})->name('list');
//Affichage détail d'annonce suivant l'id
$app->get('/annonce/:id',function($id) use($app){

	$c = new Controller();
	$c->annonceDetail($id);
	
})->name('detail');
//Verification du mot de passe de l'annonce pour la modifier
$app->post('/annonce/:id',function($id) use($app){

	$c = new Controller();
	$c->verifyMotpasse($id);
	
})->name('MotpassVerify');
//Ajouter une annonce
$app->get('/addAnnonce',function(){
	$c = new Controller();
	$c->addAnnoncePage(); 
})->name('add');

/*$app->get('/addAnnonce/preview',function(){
	$c = new Controller();
	$c->addAnnoncePreview();
})->name('addPreview');
*/

// Affichage prévisualisation annonce
$app->post('/addAnnonce/preview',function(){
    $c = new Controller();
	$c->addPreview();
})->name('addPreview');
//Enregistrement annonce
$app->post('/addAnnonce',function(){
    $c = new Controller();
	$c->addAnnonce();
})->name('addAnnonce');



$app->get('/login',function(){
	$c = new Controller();
	$c->showLogin(); 
})->name('login');
$app->post('/login',function(){
	$c = new Controller();
	$c->login(); 
})->name('authenticate');

$app->get('/logout',function(){
	$c = new Controller();
	$c->logout(); 
})->name('logout');


$app->get('/newAccount',function(){
	$c = new Controller();
	$c->showNewAccount(); 
})->name('newAccount');
$app->post('/newAccount',function(){
	$c = new Controller();
	$c->addUser(); 
})->name('addUser');
$app->get('/updateAnnonce/:id',function($id){
	$c = new Controller();
	$c->updateAnnoncePage($id); 
})->name('updateAnnonce');
$app->post('/updateAnnonce/:id',function($id){
	$c = new Controller();
	$c->updateAnnonce($id); 
})->name('updatePost');



$app->get('/myAds',function() use($app){
	
	$c = new Controller();
	$c->myAdsList($_SESSION['login']);

})->name('myAds');

// menu admin
$app->get('/admin', function(){
	$c = new Controller();
	$c->admin();
})->name('admin');
// création new admin user
$app->post('/admin', function(){
	$c = new Controller();
	$c->newAdmin();
})->name('newAdmin');
//recap new admin user
$app->get('/admin/recap', function(){
	$c = new Controller();
	$c->newAdminRecap();
})->name('newAdminRecap');

// nouvelle catégorie
$app->get('/newCat', function(){
	$c = new Controller();
	$c->newCat();
})->name('newCat');

$app->post('/newCat', function() use($app){
	$c = new Controller();
	$c->addCat();
})->name('addCat');

// API group
$app->group('/api', function () use ($app) {
	
	$authKey = function($route) use($app){
		$c = new apiController();
		$c->authorization($route);
	};

	$app->get('/annonces/apikey=:key',$authKey,function($key) use($app){
	
		$c = new apiController();
		$c->apiAnnonceList();

	})->name('apiListAds');

	$app->get('/annonce/:id/apikey=:key',$authKey,function($id,$key) use($app){
		
		$c = new apiController();
		$c->apiAnnonce($id);

	})->name('apiAdsById');

	$app->get('/categories/apikey=:key',$authKey,function($key) use($app){
	
		$c = new apiController();
		$c->apiCategoryList();

	})->name('apiListCats');
});
$app->run();