-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Sam 07 Février 2015 à 11:30
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `racoin`
--

-- --------------------------------------------------------

--
-- Structure de la table `annonce`
--

CREATE TABLE IF NOT EXISTS `annonce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(254) COLLATE utf8_bin NOT NULL,
  `title` varchar(254) COLLATE utf8_bin NOT NULL,
  `description` varchar(254) COLLATE utf8_bin NOT NULL,
  `price` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_city` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `mot_passe` varchar(224) COLLATE utf8_bin NOT NULL,
  `salt` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_category` (`id_category`,`id_city`),
  KEY `id_city` (`id_city`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=24 ;

--
-- Contenu de la table `annonce`
--

INSERT INTO `annonce` (`id`, `date`, `title`, `description`, `price`, `id_category`, `id_city`, `id_user`, `mot_passe`, `salt`) VALUES
(16, '03/02/2015', 'Pc HP', 'un super pc', 200, 1, 11, 5, '$1$BDNrTlEE$TW7g/D.jaJaIlNhV3R35k/', '$1$BDNrTlEE$MWQpb.c9OMO7RfrG0IOuc0'),
(17, '03/02/2015', 'Télévision', 'une super TV', 500, 1, 1, 6, '$1$YGZjwN.x$OonA9SVWSKBj3YN1KmLnw0', '$1$YGZjwN.x$EorJ0tyej2wQw.s4l8TCw/'),
(18, '03/02/2015', 'maison urbium iam onerosus plebeiis', 'maison urbium iam onerosus plebeiis', 250000, 3, 5, 6, '$1$l8Hzy.6w$HChkBHGom5wwSx56Sc8sv.', '$1$l8Hzy.6w$ZnVt1eiy40sBTGcyxRA9A/'),
(23, '06/02/2015', 'jjjj', 'jjjj', 1111, 1, 7, 32, '', '');

-- --------------------------------------------------------

--
-- Structure de la table `api`
--

CREATE TABLE IF NOT EXISTS `api` (
  `key` varchar(250) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `api`
--

INSERT INTO `api` (`key`) VALUES
('apikey123456789');

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `img` varchar(224) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=7 ;

--
-- Contenu de la table `category`
--

INSERT INTO `category` (`id`, `name`, `img`) VALUES
(1, 'INFORMATIQUE ET MULTIMEDIA', 'image_cat1'),
(2, 'IMMOBILIER', 'image_cat2'),
(3, 'MAISON ET JARDIN', 'image_cat3'),
(4, 'LOISIRS ET DIVERTISSEMENTS', 'image_cat4'),
(5, 'VÉHICULE', 'image_cat5'),
(6, 'EMPLOI ET SERVICES', 'image_cat6');

-- --------------------------------------------------------

--
-- Structure de la table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(254) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=15 ;

--
-- Contenu de la table `city`
--

INSERT INTO `city` (`id`, `name`) VALUES
(1, 'Nancy'),
(2, 'Metz'),
(3, 'Paris'),
(4, 'Lille'),
(5, 'Marseille'),
(6, 'Toulouse'),
(7, 'Nice'),
(8, 'Bordeaux'),
(9, 'Nantes'),
(10, 'Dijon'),
(11, ' 	Strasbourg'),
(12, 'Sète'),
(13, 'Montpellier'),
(14, 'Bastia');

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `src` varchar(254) COLLATE utf8_bin NOT NULL,
  `id_annonce` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_annonce` (`id_annonce`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Contenu de la table `image`
--

INSERT INTO `image` (`id`, `src`, `id_annonce`) VALUES
(3, 'app/views/img/annonces/10516723_840561842655038_8481994719798027561_n.jpg', 23);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `salt` varchar(255) COLLATE utf8_bin NOT NULL,
  `firstname` varchar(255) COLLATE utf8_bin NOT NULL,
  `lastname` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `phone` varchar(255) COLLATE utf8_bin NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `category` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=33 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `password`, `salt`, `firstname`, `lastname`, `email`, `phone`, `admin`, `category`) VALUES
(5, '$1$hrPpcu30$iGVyJpIWGQT0Wa84TWr4X0', '$1$hrPpcu30$csoBaKYkHQ5qdRxzXlIHz/', 'a', 'a', 'aaa@aaa.com', '000', 1, 0),
(6, '$1$uRbAHPzn$dpQaJIpwhME.PVguyG3VE/', '$1$uRbAHPzn$ifFhFLI8t4y6m9jZwcY9K1', 'user1', 'user1', 'user1@gmail.com', '1112223334', 0, 0),
(7, '$1$GyNh3Kwl$G.uz7SgLinx3Dgm66Mu9m/', '$1$GyNh3Kwl$x/xYGmumpzCZ1pvTRi9aU1', 'user2', 'user2', 'user2@gmail.com', '0001112223', 0, 0),
(9, '$1$iN/.D22.$YOKASrj0lDffBRwaUUk8M0', '$1$iN/.D22.$90Wcjqw3vaRcJNoWpaZS00', 'Martine', 'Dupuis', 'martine@lapla.ge', '0123456789', 1, 0),
(10, '$1$QK5.Zi2.$Axun3tolJq86Pi2bZhDFA0', '$1$QK5.Zi2.$j8vSZwV2O6BZkH47lxxHe.', 'zzz', 'zzz', 'zzz@z', '000', 1, 0),
(31, '$1$B71.02/.$hZM692auXn/2bUPQBLJG50', '$1$B71.02/.$5THv1Gg0.Qt9KIWzEY57F.', 'soumaya1', 'ait aazize', 'soumazize@gmail.com', '0751965562', 0, 2),
(32, '$1$5f4.oX2.$oM/9kIepMi8wUdckgURiz/', '$1$5f4.oX2.$hHARPT302siKYnIY/R3tq1', 'z', 'z', 'z@z', '44', 0, 1);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `annonce`
--
ALTER TABLE `annonce`
  ADD CONSTRAINT `annonce_ibfk_1` FOREIGN KEY (`id_category`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `annonce_ibfk_2` FOREIGN KEY (`id_city`) REFERENCES `city` (`id`),
  ADD CONSTRAINT `annonce_ibfk_3` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `image_ibfk_1` FOREIGN KEY (`id_annonce`) REFERENCES `annonce` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
